from distutils.core import setup, Extension
setup(name="dictionnaire", version="1.0",
        ext_modules=[
            Extension("dictionnaire", 
                ["dictionnaire.c", "abr.c"], 
                extra_compile_args=['-Wno-unused-function'],
                define_macros=[('NPY_NO_DEPRECATED_API', 
                                    'NPY_1_7_API_VERSION')])
            ])
