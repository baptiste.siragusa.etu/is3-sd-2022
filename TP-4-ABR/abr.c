#include <stdio.h>
#include <stdlib.h>
#include "abr.h"

void clear_abr(struct abr* A)
{
    if (A!=NIL)
    {
        clear_abr(A->gauche);
        clear_abr(A->droit);
        free(A);
    }
}

struct abr* ajouter_abr(int v, struct abr* A)
{
    struct abr* F;
    F=malloc(sizeof(struct abr));
    F->value=v;
    F->droit =NIL;
    F->gauche=NIL;
    if(A==NIL)
    {
        return F;
    }
    else
    {
        struct abr* pred;
        pred=NIL;
        struct abr *succ;
        succ=A;
        while (succ!=NIL)
        {
            pred=succ;
            if(succ->value<A->value)
                succ=succ->droit;
            else
                succ=succ->gauche;
        }
        
        if (pred->value < v)
            pred->droit=F;
        else 
            pred->gauche=F; 

        return A;
    }
}



int nombre_noeuds_abr(struct abr *A)
{
    if (A==NIL)
        return 0;
    else
        return nombre_noeuds_abr(A->gauche)+nombre_noeuds_abr(A->droit)+1;
}




int hauteur_abr(struct abr* A)
{
    if(A==NIL)
        return 0;
    else
    {
        int g;
        int d;
        g=hauteur_abr(A->gauche);
        d=hauteur_abr(A->droit);
        if (g>=d)
            return 1+g;
        else
            return 1+d;
    }
}


void afficher_abr(struct abr* A)
{
    if(A!=NIL)
    {
        afficher_abr(A->gauche);
        printf("%d ",A->value);
        afficher_abr(A->droit);
    }
        
}


void affichage_dot(struct abr * A)
{
    
    f=FILE* fopen(ABR.dot,"a+");
    
    if (A!= NIL)
    {
        affichage_dot(A->gauche);
        fprintf(f,"\t%d -> ",A->value,);
        affichage_dot(A->droit);
    }

    fclose(f);
}

void dot(FILE* f,struct abr * A)
{
    if (A->gauche !=NIL)
        fprintf(f,"\t%d -> %d [label=\"gauche\"];",A->value,A->gauche->value);
}















