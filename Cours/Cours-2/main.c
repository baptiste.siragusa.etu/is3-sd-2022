/* main.c */

#include "rationnel.h"

int main ()
{   struct rationnel A, B;

    init_rationnel (&A, 3, 4);
    init_rationnel (&B, -70, -25);
    add_rationnel (&A, &A, &B); /* A = A + B */
    print_rationnel (&A);

    clear_rationnel (&A);
    clear_rationnel (&B);
    return 0;
}

