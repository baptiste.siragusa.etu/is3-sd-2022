// Bon programme

#include <stdio.h>
#include <string.h> // pour strcpy
#include <stdlib.h> // pour malloc et free

int
main ()
{
  char *p;

  p = malloc (50 * sizeof(char)); // de la place pour 50 char
  strcpy (p, "un elephant dans un magasin de porcelaine");
  printf ("%s\n", p);
  free (p); // on libère la mémoire allouée par malloc
  return 0;
}
