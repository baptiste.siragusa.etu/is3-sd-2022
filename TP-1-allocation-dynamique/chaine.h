#include <stdlib.h>
#include <stdio.h>



/****************************************************
 * implantation
 * **************************************************/

struct chaine
{
    char* tab;    // pointe vers le tas et se termine par \0
    int i;      //nb de charactère utilisé, i<n
    int n;   //nombre de caractère aloué 
};



/****************************************************
 * Ptrototypes des fonctions
 * **************************************************/
extern void init_chaine (struct chaine*);

extern void clear_chaine (struct chaine*);

extern void ajout_char (struct chaine*, char);

extern void print_chaine (struct chaine*);
