#include <stdlib.h>
#include <stdio.h>
#include "chaine.h"

void init_chaine(struct chaine* S)
{
    S->n = 4;
    S->tab =malloc(S->n * sizeof(char));
    S-> i = 0;
    S->tab[S->i] = '\0';
}

void clear_chaine(struct chaine* S)
{
    free(S->tab);
}


extern void ajout_char (struct chaine* S, char c)
{
    if (S->i==S->n -1)
    {
        S->n +=8;
        S->tab =realloc(S,S->n*sizeof (c));
    }
    
    S->tab[S->i] = c;
    S->i += 1;
    S->tab[S->i] = '\0';
}

extern void print_chaine (struct chaine*S)
{
    printf("%s\n",S->tab);
}
