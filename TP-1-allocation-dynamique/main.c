#include <stdio.h>
#include <ctype.h>
#include "chaine.h"

int main()
{
    char c;
    struct chaine S;
    init_chaine(&S);
    c=getchar();
    
    while(! isspace(c))
    {
        ajout_char(&S, c);
        c = getchar();
    }
    
    print_chaine(&S);
    clear_chaine(&S);
    
   
   return 0; 
}
