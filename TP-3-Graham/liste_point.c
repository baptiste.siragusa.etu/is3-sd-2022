#include "liste_point.h"


void init_liste_point (struct liste_point* L)
{
    L->tete = (struct maillon_point *) 0;
    L->nbelem = 0;
}

void clear_liste_point (struct liste_point*L)
{
    struct maillon_point *courant;
    struct maillon_point *suivant;
    int i;

    courant = L->tete;
    for (i = 0; i < L->nbelem; i++)
      {
          suivant = courant->next;

          free (courant);
          courant = suivant;
      }
}


void ajouter_en_tete_liste_point (struct liste_point*L, struct point p)
{
    struct maillon_point *nouveau;

    nouveau = (struct maillon_point *) malloc (sizeof (struct maillon_point));
    assert (nouveau != (struct maillon_point *) 0);
/* appeler ici un éventuel constructeur pour nouveau->value */
    nouveau->point = p;         /* affectation de la valeur */
    nouveau->next = L->tete;
    L->tete = nouveau;
    L->nbelem += 1;
}


void extraire_tete_liste_point (struct liste_point*L, struct point* p)
{
    struct maillon_point *tete;

    assert (L->nbelem != 0);
    tete = L->tete;
    *p = tete->point;           /* affectation */
    L->tete = tete->next;
    L->nbelem -= 1;
    free (tete);
}










