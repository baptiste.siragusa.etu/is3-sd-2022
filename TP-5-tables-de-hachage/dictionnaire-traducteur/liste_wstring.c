
#include <wchar.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "liste_double.h"

void
init_liste_wstring (struct liste_wstring *L)
{
    L->tete = (struct maillon_wstring *) 0;
    L->nbelem = 0;
}

void
ajouter_en_tete_liste_wstring (struct liste_wstring *L, wstring cle, wstring sat)
{
    struct maillon_wstring *nouveau;

    nouveau =
        (struct maillon_wstring *) malloc (sizeof (struct maillon_wstring));
    assert (nouveau != (struct maillon_wstring *) 0);
/* appeler ici un éventuel constructeur pour nouveau->value */
    nouveau->clef = cle;         /* affectation de la valeur */
    nouveau->satellite = sat;
    nouveau->next = L->tete;
    L->tete = nouveau;
    L->nbelem += 1;
}

void
clear_liste_wstring (struct liste_wstring *L)
{
    struct maillon_wstring *courant;
    struct maillon_wstring *suivant;
    int i;

    courant = L->tete;
    for (i = 0; i < L->nbelem; i++)
      {
          suivant = courant->next;
/* appeler ici un éventuel destructeur pour nouveau->value */
          free (courant);
          courant = suivant;
      }
}

/*
 * Sous-fonction de set_liste_double.
 * Si L = [m1, m2, ..., mn] avant l'appel alors
 *    L = [mn, ..., m2, m1] après l'appel.
 * Aucune allocation dynamique n'est effectuée. 
 * Seuls les pointeurs sont modifiés
 */

static void
retourner_liste_wstring (struct liste_wstring *L)
{
    struct maillon_wstring *precedent, *courant, *suivant;
    int i;

    if (L->nbelem >= 2)
      {
          courant = L->tete;
          suivant = courant->next;
          courant->next = (struct maillon_wstring *) 0;
          for (i = 1; i < L->nbelem; i++)
            {
                precedent = courant;
                courant = suivant;
                suivant = suivant->next;
                courant->next = precedent;
            }
          L->tete = courant;
      }
}

void
set_liste_wstring (struct liste_wstring *dst, struct liste_wstring *src)
{
    struct maillon_double *M;
    int i;

    if (dst != src)
      {
          clear_liste_double (dst);
          init_liste_double (dst);
          M = src->tete;
          for (i = 0; i < src->nbelem; i++)
            {
                ajouter_en_tete_liste_wstring (dst, M->clef,M->satellite);
                M = M->next;
            }
          retourner_liste_wstring (dst);
      }
}

void
extraire_tete_liste_double (double *d, struct liste_double *L)
{
    struct maillon_double *tete;

    assert (L->nbelem != 0);
    tete = L->tete;
    *d = tete->value;           /* affectation */
    L->tete = tete->next;
    L->nbelem -= 1;
    free (tete);
}

void
imprimer_liste_double (struct liste_double *L)
{
    struct maillon_double *M;
    int i;

    printf ("[");
    M = L->tete;
    for (i = 0; i < L->nbelem; i++)
      {
          if (i == 0)
              printf ("%f", M->value);
          else
              printf (", %f", M->value);
          M = M->next;
      }
    printf ("]\n");
}


bool recherche_liste(struct liste_double *L, double val)
{
    bool b = false;
    struct maillon_double *M;
    int i;
    M = L->tete;
    for (i = 0; i < L->nbelem; i++)
      {
          if (M->value == val)
          {
              b= true;
          }
          M = M->next;
      }
      return b;
}

















