#include "hachage_simple.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

void init_table(struct table* T, fonction_hachage* h)
{
    for (int i=0;i<10;i++)
    {
        init_liste_double (&T->tab[i].L);
    }
    T->hash=h;
}


void clear_table (struct table *T)
{
     for (int i=0;i<10;i++)
    {
        clear_liste_double (&T->tab[i].L);
    }
}


void enregistrer_table (struct table *T, double val)
{
    int i;
    i=T->hash(val);
    ajouter_en_tete_liste_double (&T->tab[i].L, val);
}


bool rechercher_table (struct table *T, double val)
{
    bool b = false;
    int i;
    i=T->hash(val);
    if (recherche_liste(&T->tab[i].L, val)==true)
    {
        b=true;
    }
    return b;
}


void imprimer_table (struct table *T)
{
     for (int i=0;i<10;i++)
    {
        printf("alveole n°%d\n",i);
        imprimer_liste_double (&T->tab[i].L);
        
    }
}










